package com.schoolofnet.junit_project;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Classe respons�vel por verificar se um n�mero � divis�vel por 2
 */
public class DivisibleBy extends TypeSafeDiagnosingMatcher<Integer>{
	
	private final Integer number;
	
	public DivisibleBy(Integer number){
		this.number = number;
	}
	
	public static DivisibleBy divisibleBy(Integer number){
		return new DivisibleBy(number);
	}

	/**
	 * Descri��o do que est� sendo checado caso o m�todo falhe
	 */
	public void describeTo(Description description) {
		description.appendText("Checking if the number is divisible by 2.");
	}

	/**
	 * Realiza a opera��o
	 */
	@Override
	protected boolean matchesSafely(Integer number, Description mismatchDescription) {
		Integer result = number % 2;
		mismatchDescription.appendText("Number ").appendValue(number).appendText("is not divisible by 2. ").appendText("Remain ").appendValue(result);
		return result == 0;
	}

}
