package com.schoolofnet.junit_project;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

/**
 * Classe respons�vel por verificar se uma String � vazia
 */
public class IsEmptyString extends TypeSafeDiagnosingMatcher<String>{

	private final String str;
	
	public IsEmptyString(String str){
		this.str = str;
	}
	
	public static IsEmptyString isEmpty(String str){
		return new IsEmptyString(str);
	}
	
	/**
	 * Descri��o do que est� sendo checado caso o m�todo falhe
	 */
	public void describeTo(Description description) {
		description.appendText("Checking String empty.");
	}

	/**
	 * Realiza a compara��o de Strings
	 */
	@Override
	protected boolean matchesSafely(String str, Description mismatchDescription) {
		Boolean result = str != "";
		mismatchDescription.appendText("A String ").appendValue(str).appendText(" is empty").appendValue(result);
		return result;
	}
		

}
