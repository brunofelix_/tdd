package com.schoolofnet.junit_project;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Classe de testes de StringUtils
 */
public class StringUtilsTest{
	
	@Test
	public void testConcatenate(){
		
		StringUtils stringUtils = new StringUtils();
		
		final String resultMethod = stringUtils.concatenate("Tests with ", "jUnit");
		final String resultExpected = "Tests with jUnit"; 
		
		//	Confirma se o resultado do m�todo testado � o mesmo do resultado esperado
		assertEquals(resultExpected, resultMethod);
		
	}

}
