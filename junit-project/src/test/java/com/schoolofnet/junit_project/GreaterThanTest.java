package com.schoolofnet.junit_project;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Parametrizando testes
 */
@RunWith(Parameterized.class)
public class GreaterThanTest {
	
	private GreaterThan validate;
	private Boolean result;
	private Integer inputNumber;
	
	public GreaterThanTest(Integer inputNumber, Boolean result){
		this.inputNumber = inputNumber;
		this.result = result;
	}

	/**
	 * Istancia minha classe antes de cada teste
	 */
	@Before
	public void initialize(){
		this.validate = new GreaterThan();
	}
	
	
	@Parameters
	public static Collection setNumbers(){
		return Arrays.asList(new Object[][]{
			{10, true},
			{50, true},
			{1, false},
			{2, false},
			{4, false}
		});
	}
	
	@Test
	public void greaterThanTest(){
		System.out.println("Testing...");
		assertEquals(this.result, validate.validade(this.inputNumber));
	}
	
	
	
	/**
	 * Destr�i minha inst�ncia ap�s cada teste
	 */
	@After
	public void close(){
		this.validate = null;
	}
	
	
}
