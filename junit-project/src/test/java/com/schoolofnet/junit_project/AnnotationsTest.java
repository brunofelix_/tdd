package com.schoolofnet.junit_project;

import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 * Classe de testes de annotations
 */
public class AnnotationsTest {
	
	/**
	 * Inicia antes dos testes @Test
	 */
	@BeforeClass
	public static void beforeClassTest(){
		System.out.println("@BeforeClass executed.");
	}
	
	/**
	 * Inicia ap�s os testes @Test
	 */
	@AfterClass
	public static void afterClassTest(){
		System.out.println("@AfterClass executed.");
	}
	
	/**
	 * Executa alguma a��o antes de cada @Test
	 */
	@Before
	public void beforeTest(){
		System.out.println("@Before executed.");
	}
	
	/**
	 * Executa alguma a��o ap�s cada @Test
	 */
	@After
	public void afterTest(){
		System.out.println("@After executed.");
	}
	
	/**
	 * Ignora a execu��o de um caso de teste
	 */
	@Test
	@Ignore
	public void ignoreTest(){
		System.out.println("@Ignore executed.");
	}
	
	/**
	 * Quando eu j� espero determinado erro
	 */
	@Test(expected = ArithmeticException.class)
	public void expectedTest(){
		Integer number = 10 / 0;
	}
	
	/**
	 * Intercepta o m�todo testado, executa e desfaz tudo que ele havia realizado
	 */
	@Rule
	public TemporaryFolder folder = new TemporaryFolder();
	
	@Test
	public void createFolderRuleTest() throws IOException{
		File folder = this.folder.newFolder("MY_FOLDER");
		assertTrue(folder.exists());
	}
	
	@Test
	public void testOneTest(){
		System.out.println("@Test one");
	}
	
	@Test
	public void testTwoTest(){
		System.out.println("@Test two");
	}
	
	

}
