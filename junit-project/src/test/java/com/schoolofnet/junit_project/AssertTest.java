package com.schoolofnet.junit_project;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static com.schoolofnet.junit_project.IsEmptyString.*;
import static com.schoolofnet.junit_project.DivisibleBy.*;

public class AssertTest {
	
	@Test
	public void assertTest(){
		
		final String str1 = "jUnit";
		final String str2 = "jUnit";
		final Integer int1 = 1;
		final Integer int2 = 1;
		final String isNull = null;
		final Boolean isFalse = false;
		final Boolean isTrue = true;
		final Integer[] array1 = {5, 10, 15};
		final Integer[] array2 = {0, 0, 0};
		final String strTest = "";
		final Integer numberTest = 4;
		
		// Verifica se � igual
		assertEquals(str1, str2);
		assertSame(int1, int2);
		
		// Verifica se s�o diferentes
		assertNotSame(str1, str2);
		assertNotEquals(int1, int2);
		
		// Verifica se � nulo ou n�o (Se for nulo falha)
		assertNotNull(isNull);
		
		// Verifica se � nulo ou n�o (Se for nulo passa)
		assertNull(isNull);
		
		// Verifica se � verdadeiro (Se for falso falha)
		assertTrue(isTrue);
		
		// Verifica se o length de um array � igual ao outro (Ele n�o avalia os valores e sim o tamanho dos arrays)
		assertArrayEquals(array1, array2);
		
		// Verifica qualquer compara��o que eu quiser fazer
		assertThat(str1, isA(String.class));
		assertThat(str1, is("jUnit"));
		assertThat("Not equals", "123", is("321"));
		
		//	Verifica se uma String � vazia da clsse IsEmptyString
		assertThat(strTest, is(isEmpty(strTest)));
		
		//	Verifica se um n�mero � divis�vel por 2 da classe DivisibleBy
		assertThat(numberTest, is(divisibleBy(numberTest)));
		
	}

}
