package com.schoolofnet.junit_project;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Executa v�rias classes de testes
 */
@RunWith(Suite.class)
@SuiteClasses({
	AnnotationsTest.class,
	AssertTest.class,
	StringUtilsTest.class
})
public class RunSuiteTest {

	
}
