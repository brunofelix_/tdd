package com.schoolofnet.junit_project;

import org.junit.Test;
import org.junit.runners.MethodSorters;

import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import static org.hamcrest.CoreMatchers.*;

/**
 * Ordena a ordem de execu��o dos testes
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SuiteCaseTest2 {
	
	@Test
	public void subTest(){
		assertThat(2 - 2, is(0));
	}
	
	@Test
	public void multTest(){
		assertThat(2 * 2, is(4));
	}

}
