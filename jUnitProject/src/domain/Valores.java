package domain;

import java.util.Arrays;

public class Valores implements IValores {

	public int[] valores = new int[10];

	public Valores() {

	}

	@Override
	public boolean ins(int v) {
		if (v <= 0) {
			return false;
		}
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] == 0) {
				valores[i] = v;
				return true;
			}
		}
		return false;
	}

	@Override
	public int del(int i) {
		if (i < 0) {
			return -1;
		}
		int valorRetornar;		
		if (valores[i] != 0) {
			valorRetornar = valores[i];
			valores[i] = 0;
			return valorRetornar;
		}
		return -1;
	}

	@Override
	public int size() {
		int size = 0;
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] != 0) {
				size++;
			}
		}
		return size;
	}

	@Override
	public double mean() {
		int sum = 0;
		for (int i = 0; i < valores.length; i++) {
			sum = sum + valores[i];
		}
		double mean = sum / valores.length;
		return mean;
	}

	@Override
	public int greater() {
		return Arrays.stream(valores).max().getAsInt();
	}

	@Override
	public int lower() {
		return Arrays.stream(valores).min().getAsInt();
	}

	public void remove(int y) {
		int[] vetorAux = new int[10];
		int index = 0;
		for (int i = 0; i < valores.length; i++) {
			if (valores[i] != y) {
				vetorAux[index] = valores[i];
				index++;
			}
		}
		valores = vetorAux;
	}

}
